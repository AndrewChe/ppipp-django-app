from django.db import models
from django.contrib.auth.models import User


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


# Create your models here.
class Publication(models.Model):
    name = models.CharField(max_length=100)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField()
    document = models.FileField(upload_to='media', validators=[validate_file_extension])

    def __str__(self):
        return str(self.name) + '-' + str(self.author)
