from django.shortcuts import render, redirect
from Publications.models import Publication


# Create your views here.
def all_publications(request):
    publications = Publication.objects.all()
    return render(request, 'AllPublications.html', {'publications': publications})


def publication(request, pub_id):
    pub = Publication.objects.get(id=pub_id)
    return render(request, 'Publication.html', {'publication': pub})


def add_publication(request):
    if request.method == 'POST':
        new_publication = Publication(name=request.POST['name'],
                                      description=request.POST['description'],
                                      author=request.user,
                                      document=request.FILES['pub_file']
                                      )
        new_publication.save()
        return redirect('/all_publications/')
    if request.user.user_info.user_type != 1:
        return redirect('/all_publications/')
    return render(request, 'AddPublication.html')
