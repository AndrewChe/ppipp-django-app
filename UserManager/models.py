from django.db import models
from django.contrib.auth.models import User


class UserTypes(models.IntegerChoices):
    VIEWER = 0, 'Читатель'
    WRITER = 1, 'Автор'


# Create your models here.
class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_info')
    user_type = models.IntegerField(default=UserTypes.VIEWER, choices=UserTypes.choices)

    def __str__(self):
        return str(self.user)
