from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from UserManager.models import UserInfo, UserTypes


# Create your views here.
def auth(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is None:
            return redirect('/auth/')
        login(request, user)
        return redirect('/')
    return render(request, 'Auth.html')


def register(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        last_name = request.POST['lastname']
        first_name = request.POST['firstname']
        email = request.POST['email']
        if User.objects.filter(username=username).exists():
            return redirect('/registration/')
        if User.objects.filter(email=email).exists():
            return redirect('/registration/')
        try:
            validate_email(email)
        except ValidationError:
            return redirect('/registration/')
        new_user = User.objects.create_user(username, email, password, first_name=first_name, last_name=last_name)
        new_user_info = UserInfo(user=new_user, user_type=UserTypes.VIEWER)
        new_user.save()
        new_user_info.save()
        return redirect('/auth/')
    return render(request, 'Register.html')


def logout_view(request):
    logout(request)
    return redirect('/auth/')


def home_page(request):
    # if request.user.is_authenticated:
    return redirect('/all_publications/')
    # return render(request, 'HomePage.html')
